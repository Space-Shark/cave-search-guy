﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	Vector3 originalPosition;
	Quaternion originalRotation;
	float originalOrthoSize;
	Camera thisCamera;

	public bool autoCamera = false;

	protected void Setup() {
		thisCamera = GetComponent <Camera>();
		originalPosition = thisCamera.transform.position;
		originalRotation = thisCamera.transform.rotation;
		originalOrthoSize = thisCamera.orthographicSize;
	}

	protected void SetupCamera() {
		thisCamera.transform.position = originalPosition;
		thisCamera.transform.rotation = originalRotation;
		thisCamera.orthographicSize = originalOrthoSize;
	}

	public void TurnOn() {
		thisCamera.enabled = true;
		gameObject.SetActive (true);
	}

	public void TurnOff() {
		thisCamera.enabled = false;
		gameObject.SetActive (false);
	}
}
