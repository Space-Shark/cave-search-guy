﻿using UnityEngine;
using System.Collections;

public class PlayerController2D : MonoBehaviour {

	Rigidbody2D rb;
	Vector2 velocity;

	// Use this for initialization
	void Awake () {
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		velocity = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical")).normalized * 10;
	}

	void FixedUpdate() {
		rb.MovePosition (rb.position + velocity * Time.fixedDeltaTime);
	}

	public void TurnOn() {
		gameObject.SetActive (true);
		rb.isKinematic = false;
	}

	public void TurnOff() {
		gameObject.SetActive (false);
		rb.isKinematic = true;
	}

}
