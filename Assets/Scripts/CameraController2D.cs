﻿using UnityEngine;
using System.Collections;

public class CameraController2D : CameraController {

	private PlayerController2D player;

	private Vector3 lastPlayerPosition;
	private float distanceToMoveX;
	private float distanceToMoveY;

	// Use this for initialization
	void Start () {
		base.Setup ();
		SetupCamera ();
	}

	// Update is called once per frame
	void Update () {
		if (!autoCamera) {
			distanceToMoveX = player.transform.position.x - lastPlayerPosition.x;
			distanceToMoveY = player.transform.position.y - lastPlayerPosition.y;

			transform.position = new Vector3 (transform.position.x + distanceToMoveX, transform.position.y + distanceToMoveY, transform.position.z);

			lastPlayerPosition = player.transform.position;
		}
	}

	new public void SetupCamera() {
		base.SetupCamera ();
		if (!autoCamera) {
			player = FindObjectOfType<PlayerController2D> ();
			lastPlayerPosition = player.transform.position;
			distanceToMoveX = 0.0f;
			distanceToMoveY = 0.0f;
			transform.position = new Vector3 (lastPlayerPosition.x, lastPlayerPosition.y, transform.position.z);
		}
	}
}
