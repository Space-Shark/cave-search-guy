﻿using UnityEngine;
using System.Collections;

public class PlayerController3D : MonoBehaviour {

	Rigidbody rb;
	Vector3 velocity;

	// Use this for initialization
	void Awake () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		velocity = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized * 10;
	}

	void FixedUpdate() {
		rb.MovePosition (rb.position + velocity * Time.fixedDeltaTime);
	}

	public void TurnOn() {
		gameObject.SetActive (true);
		rb.isKinematic = false;
	}

	public void TurnOff() {
		gameObject.SetActive (false);
		rb.isKinematic = true;
	}

}
