﻿using UnityEngine;
using System.Collections;

public class CameraController3D : CameraController {

	private PlayerController3D player;

	private Vector3 lastPlayerPosition;
	private float distanceToMoveX;
	private float distanceToMoveZ;

	// Use this for initialization
	void Start () {
		base.Setup ();
		SetupCamera ();
	}

	// Update is called once per frame
	void Update () {
		if (!autoCamera) {
			distanceToMoveX = player.transform.position.x - lastPlayerPosition.x;
			distanceToMoveZ = player.transform.position.z - lastPlayerPosition.z;

			transform.position = new Vector3 (transform.position.x + distanceToMoveX, transform.position.y, transform.position.z + distanceToMoveZ);

			lastPlayerPosition = player.transform.position;
		}
	}

	new public void SetupCamera() {
		base.SetupCamera ();
		if (!autoCamera) {
			player = FindObjectOfType<PlayerController3D> ();
			lastPlayerPosition = player.transform.position;
			distanceToMoveX = 0.0f;
			distanceToMoveZ = 0.0f;
			transform.position = new Vector3 (lastPlayerPosition.x, transform.position.y, lastPlayerPosition.z + transform.position.z);
		}
	}
}
